# Pandemics recipe: default PDF

This repo contains the default Pandemics recipe for PDF format.
It is nothing more than a wrapper of the excellent [Eisvogel template](https://github.com/Wandmalfarbe/pandoc-latex-template).
Although you could use the original template directly, this version make use of the instruction system of Pandemics to tune the template a bit (switch on the title page with custom colors, listings).

As PDF is the default format and this is the default recipe for PDF, you have nothing to do to use this recipe!

The default template is best suited for structured reports (eg. a thesis).
For a short report (less spacing, no table of contents or section numbers), use the short format:

```
---
pandemics:
  format: short.pdf
---
```

If you only want to export a couple of pages, you can use the
handout format (like short but without the title page):

```
---
pandemics:
  format: handout.pdf
---
```

If you want to explicitly reference this recipe, eg. to use an older version, just add in the YAML front-matter of your markdown document:

```
---
pandemics:
  recipe: gitlab:pandemics/recipe-default-pdf SHA
---
```

where `SHA` is the commit number of the version you want to use.
