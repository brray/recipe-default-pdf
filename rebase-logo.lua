pandoc.utils = require 'pandoc.utils'

function Meta(m)
  if m["titlepage-logo"] ~= nil
  then
    rebasedlogo = os.getenv('PANDOC_SOURCE_PATH') .. '/' .. pandoc.utils.stringify(m["titlepage-logo"])
    m["titlepage-logo"] = pandoc.MetaInlines(rebasedlogo)
  end
  return m
end
